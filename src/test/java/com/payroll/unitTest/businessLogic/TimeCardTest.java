package com.payroll.unitTest.businessLogic;

import com.payroll.businessLogic.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimeCardTest {
    @Test
    public void testMorningOnly() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/04/11 09:00:00", "2014/04/11 12:00:00");

        // Act
        timecard.process();

        // Assert
        int expectedBaseHour = 3;
        int expectedOverTime = 0;
        assertEquals(timecard.getBaseHours(), expectedBaseHour);
        assertEquals(timecard.getOverTimeHours(), expectedOverTime);
    }

    @Test
    public void testFullDayNoOTOnly() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/04/11 09:00:00", "2014/04/11 18:00:00");

        // Act
        timecard.process();

        // Assert
        int expectedBaseHour = 8;
        int expectedOverTime = 0;
        assertEquals(expectedBaseHour, timecard.getBaseHours());
        assertEquals(expectedOverTime, timecard.getOverTimeHours());
    }

    @Test
    public void testFullDayWithOT() {
        // Arrange
        TimeCard timecard = new TimeCard("2014/04/11 10:00:00", "2014/04/11 22:00:00");

        // Act
        timecard.process();

        // Assert
        /*int expectedBaseHour = 11;
        int expectedOverTime = 0;
        */
        int expectedBaseHour = 8;
        int expectedOverTime = 3;

        assertEquals(expectedBaseHour, timecard.getBaseHours());
        assertEquals(expectedOverTime, timecard.getOverTimeHours());
    }

    @Test
    public void testWorkingOnHoliday() {
        // Arrange
        TimeCard timecard = new TimeCard("2018/12/01 9:00:00", "2018/12/01 17:00:00");

        // Act
        timecard.process();

        // Assert
        /*int expectedBaseHour = 7;
        int expectedOverTime = 0;
        */
        int expectedBaseHour = 0;
        int expectedOverTime = 7;

        assertEquals(expectedBaseHour, timecard.getBaseHours());
        assertEquals(expectedOverTime, timecard.getOverTimeHours());
    }

    @Test
    public void testBroken() {
        assertEquals(true, true);
    }
}
